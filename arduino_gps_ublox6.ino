#include <string.h>
#include <SoftwareSerial.h>

//Hardware Config
const int rxPin = 2;                    // RX PIN 
const int txPin = 4;                    // TX TX
const int baudRate = 9600;

//Serial Buffer
const int lineBuffer = 512;
int lineIdx = 0;
char line[lineBuffer] = "";
SoftwareSerial gpsSerial(rxPin, txPin);

//Variables
float lat;
char ns;
float lon;
char ew;
int satCount;
int utcDay;
int utcMonth;
int utcYear;
int utcHours;
int utcMinutes;
float utcSeconds;
int pollInterval = 0;

int toHex(char h){
  switch (h){
    case '0': return 0;
    case '1': return 1;
    case '2': return 2;
    case '3': return 3;
    case '4': return 4;
    case '5': return 5;
    case '6': return 6;
    case '7': return 7;
    case '8': return 8;
    case '9': return 9;
    case 'A': return 10;
    case 'B': return 11;
    case 'C': return 12;
    case 'D': return 13;
    case 'E': return 14;
    case 'F': return 15;
  }
  return 0;
}

void convertToTime(String value, int &hours, int &minutes, float &seconds){
  hours = value.substring(0,2).toInt();
  minutes = value.substring(2,4).toInt();
  char buffer[10];
  value.substring(4,value.length()).toCharArray(buffer,10);
  seconds = atof(buffer);
}

float convertToDegrees(String value,int degLen){
  char buffer[20];
  value.substring(0,degLen).toCharArray(buffer,20);
  int deg = atoi(buffer);
  value.substring(degLen,value.length()).toCharArray(buffer,20);
  float minutes = atof(buffer);
  float r = deg + (minutes/60);
  return r;
}

boolean verifyMsg(char* msg){
  int startIdx = -1;
  int endIdx = -1;
  for(int i=0;i<lineBuffer;i++){
    if (msg[i]=='\0') break;
    else if(msg[i]=='$') startIdx=i+1;
    else if(msg[i]=='*') endIdx=i;
  }
  if(startIdx==-1||endIdx==-1){
    return false;
  }
  else{
    int c = 0;
    for(int i=startIdx;i<endIdx;i++){
      c = c ^ msg[i];
    }
    int checksum = toHex(msg[endIdx+1]);
    checksum = (checksum<<4)|toHex(msg[endIdx+2]);
    if (checksum==c){
      return true;
    }
  }
  return false;
}

void processMsg(char* msg){
  String line = String(msg);
//  Serial.println(line);
  line = line.substring(line.indexOf('$'),line.lastIndexOf('*'));
  int dollarPos = 0;
  do{
    int posEnd = line.indexOf('$',dollarPos+1);
    String l = "";
    if (posEnd==-1)l=line;
    else l=line.substring(dollarPos,posEnd);

    String msgId = l.substring(0,6);    
    if (msgId.equals("$GPGGA"))processGPGGA(l);
    else if (msgId.equals("$GPGLL"))processGPGLL(l);
    else if (msgId.equals("$GPZDA"))processGPZDA(l);
    dollarPos = posEnd;
  }while(dollarPos!=-1);
  Serial.flush();
}

void processGPGGA(String line){
  int pos = 0;
  int commaPos = 0;
  int fieldNumber = 0;
  
  //temp
  float lat_t;
  char ns_t;
  float lon_t;
  char ew_t;
  int satCount_t;
  int utcHours_t;
  int utcMinutes_t;
  float utcSeconds_t;

  while(commaPos!=-1){
    commaPos = line.indexOf(',',pos);
    String arg = line.substring(pos,commaPos);
    if(commaPos==-1)line.substring(pos,line.length());
    switch (fieldNumber){
      case 1:
        convertToTime(arg,utcHours_t,utcMinutes_t,utcSeconds_t);
        break;
      case 2:
        lat_t = convertToDegrees(arg,2);
        break;
      case 3:
        ns_t = arg[0];
        break;
      case 4:
        lon_t = convertToDegrees(arg,3);
        break;
      case 5:
        ew_t = arg[0];
        break;
      case 7:
        satCount_t = arg.toInt();
        break;
    }
    pos = commaPos + 1;
    fieldNumber++;
  }
  if(fieldNumber==15){
      lat = lat_t;
      ns = ns_t;
      lon = lon_t;
      ew = ew_t;
      satCount = satCount_t;
      utcHours = utcHours_t;
      utcMinutes = utcMinutes_t;
      utcSeconds = utcSeconds_t;
      printGPS();
  }
}

void processGPGLL(String line){
  int pos = 0;
  int commaPos = 0;
  int fieldNumber = 0;
  
  //temp
  float lat_t;
  char ns_t;
  float lon_t;
  char ew_t;
  int utcHours_t;
  int utcMinutes_t;
  float utcSeconds_t;
  char status_t;

  while(commaPos!=-1){
    commaPos = line.indexOf(',',pos);
    String arg = line.substring(pos,commaPos);
    if(commaPos==-1)line.substring(pos,line.length());
    switch (fieldNumber){
      case 1:
        lat_t = convertToDegrees(arg,2);
        break;
      case 2:
        ns_t = arg[0];
        break;
      case 3:
        lon_t = convertToDegrees(arg,3);
        break;
      case 4:
        ew_t = arg[0];
        break;
      case 5:
        convertToTime(arg,utcHours_t,utcMinutes_t,utcSeconds_t);
        break;
      case 6:
        status_t = arg[0];
    }
    pos = commaPos + 1;
    fieldNumber++;
  }
  if(fieldNumber>6){
    if (status_t=='A'){
      lat = lat_t;
      ns = ns_t;
      lon = lon_t;
      ew = ew_t;
      utcHours = utcHours_t;
      utcMinutes = utcMinutes_t;
      utcSeconds = utcSeconds_t;
      printGPS();
    }
  }
}

void processGPZDA(String line){
  Serial.println(line);
  int pos = 0;
  int commaPos = 0;
  int fieldNumber = 0;
  
  //temp
  int utcDay_t;
  int utcMonth_t;
  int utcYear_t;
  int utcHours_t;
  int utcMinutes_t;
  float utcSeconds_t;

  while(commaPos!=-1){
    commaPos = line.indexOf(',',pos);
    String arg = line.substring(pos,commaPos);
    if(commaPos==-1)line.substring(pos,line.length());
    switch (fieldNumber){
      case 1:
        convertToTime(arg,utcHours_t,utcMinutes_t,utcSeconds_t);
        break;
      case 2:
        utcDay_t = arg.toInt();
        break;
      case 3:
        utcMonth_t = arg.toInt();
        break;
      case 4:
        utcYear_t = arg.toInt();
        break;
    }
    pos = commaPos + 1;
    fieldNumber++;
  }
  if(fieldNumber==7){
    utcDay = utcDay_t;
    utcMonth = utcMonth_t;
    utcYear = utcYear_t;
    utcHours = utcHours_t;
    utcMinutes = utcMinutes_t;
    utcSeconds = utcSeconds_t;
    printGPS();
  }
}

void requestZDA(){
  char msg[] = "$GPGPQ,ZDA*22\r\n";
  gpsSerial.write(msg);
}

void printGPS(){
  Serial.print("lat: ");
  Serial.print(lat,8);
  Serial.print(ns);
  Serial.print("  long: ");
  Serial.print(lon,8);
  Serial.print(ew);
  Serial.println("");
  Serial.print("number of sats: ");
  Serial.println(satCount);
  Serial.print("date: ");
  Serial.print(utcDay);
  Serial.print("/");
  Serial.print(utcMonth);
  Serial.print("/");
  Serial.print(utcYear);
  Serial.print("  time: ");
  Serial.print(utcHours);
  Serial.print(":");
  Serial.print(utcMinutes);
  Serial.print(":");
  Serial.print(utcSeconds,3);
  Serial.println("");
  Serial.println("");
  Serial.flush();
}

void setup(){
   pinMode(rxPin, INPUT);
   pinMode(txPin, OUTPUT);
   for(int i=0;i<lineBuffer;i++)line[i]=' ';
   gpsSerial.begin(baudRate);
   Serial.begin(baudRate);
   Serial.flush();
   Serial.println("RESET COMPLETE");
   requestZDA();
}

void loop(){
  if(gpsSerial.available()>0){
    int charIn = gpsSerial.read();
    if(charIn!=13){
      if(charIn==10){
        line[lineIdx] = '\0';
        boolean msgOK = verifyMsg(line);
        if(msgOK==true){
          processMsg(line);
        }
        for(int i=0;i<lineBuffer;i++)line[i]=' ';
        lineIdx=0;
        pollInterval++;
        if(pollInterval>30){
          requestZDA();
          pollInterval = 0;
        }
      }
      else{
        //buffer overflow
        line[lineIdx] = charIn;
        lineIdx++;
      }
    }
  }
}
